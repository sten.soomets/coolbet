# coding: utf-8

#member_count, sum1, sum2, sum3, sum4, sum5

data = [
    (2, 2, 3, 6, 5, 4, ),
    (4, 4, 11, 7, 13, 5),
    (3, 4, 3, 8, 9, 5),
]

for i, x in enumerate(data, start=1):
    memcount = x[0]
    sums = x[1:]
    maxcount = (memcount * 3)
    results = []
    for result_sum in sums:
        offset_result_sum = float(result_sum) - memcount
        offset_max_count = maxcount - memcount
        value = offset_result_sum / offset_max_count
        flipped_numb = 1 - value
        percentage = flipped_numb * 100
        final_percentage = round(percentage, 2)
        if final_percentage < 0:
            results.append('Error: Sum less than min')
        elif final_percentage > 100:
            results.append('Error: Sum greater than max')
        else:
            results.append(final_percentage)
    print ("--------------------------------------------------------")
    print('row %i with %i members: %s' % (i, memcount, results))
